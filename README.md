
# Booking service

This project is a Java Spring Boot server application providing : 

* A REST API controller
* A Blockchain API controller
* Domain Driven Design

## Blockchain

The communication with the Ethereum blockchain is supported by Web3j and the Infura API. Thats is why you should provide two environment variables at startup : 
* INFURA_API_KEY
* WALLET_PRIVATE_KEY

There is also the BookingContract class generated with Web3j wich permits to interact with the SmartContract located at 0xf7396a3542e8ed994fb1a29d85235f1b6d4bedc7 on the Rinkeby network

## REST Api

### Get rooms
***(GET) /getRooms*** will return a list containing all room IDs stored in the smart contract.

### Get bookins

***(POST) /getBookings*** will return a list a timeslots.

The first request will return all booked timeslots of the room
```javascript
{
	"roomId": "C01" 
}
```
The second request will return all booked timeslots of the room by the given user.
```javascript
{
	"roomId": "C05",
	"userEmail" : "user1@email.com"
}
```

### Add a booking
***(POST) /add*** will return the status of the booking.
It can be business errors from the server or from the transaction on the blockchain. Or a success message.
```javascript
{
	"userEmail":"user1@email.com",
	"roomId":"C01",
	"timeSlot":"13"
}
```


### Cancel a booking
***(POST) /cancel*** will return the status of the request.
It can be business errors from the server or from the transaction on the blockchain. Or a success message.
```javascript
{
	"userEmail":"user1@email.com",
	"roomId":"C01",
	"timeSlot":"13"
}
```
## How to use it

### Prerequisites

* JRE and JDK
* Gradle
 
### Without Docker

Build the project by executing this command from the root folder :

```
./gradlew build
```
And run by executing the jar located in the boot subproject :

```
java "-DINFURA_API_KEY=01020304" "-DWALLET_PRIVATE_KEY=01020304" \
 -jar boot/build/libs/booking-server.jar
```

The server will be available at http://localhost:8080/ 

### With Docker

You can create a docker image by calling the Dockerfile using :
```
docker build -t bookingservice .
```
And then running the image by executing :

```
docker run -p 8080:8080 -d bookingservice \
-e "INFURA_API_KEY=01020304" -e "WALLET_PRIVATE_KEY=01020304"  
```
The server will be available at http://\<docker_url\>:8080/ 


