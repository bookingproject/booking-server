FROM openjdk:8-jdk
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=boot/build/libs/booking-server.jar

# Add the application's jar to the container
ADD ${JAR_FILE} booking-server.jar

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/booking-server.jar"]




