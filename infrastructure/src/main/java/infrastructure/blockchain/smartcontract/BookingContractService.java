package infrastructure.blockchain.smartcontract;

import domain.booking.blockchain.ContractService;
import infrastructure.blockchain.smartcontract.exception.ContractNotInitialized;
import infrastructure.blockchain.smartcontract.utils.StringUtils;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.stereotype.Component;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Hash;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookingContractService implements ContractService {

    private final Web3j web3j;
    private final Credentials credentials;

    private BookingContract contract;

    public BookingContractService(Web3j web3j, Credentials credentials) {
        this.web3j = web3j;
        this.credentials = credentials;
        loadContract(this.credentials);
    }


    public void loadContract(Credentials credentials){
        contract = BookingContract.load(
                BookingContract.getPreviouslyDeployedAddress("4"),
                this.web3j,
                credentials,
                ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
    }

    @Override
    public TransactionReceipt addBooking(String roomId, String timeSlot, String userId) throws Exception{
        if(contract == null){
            throw new ContractNotInitialized();
        }
        return contract.addBooking(StringUtils.stringToBytes32(roomId), new BigInteger(timeSlot), Hash.sha3(userId.getBytes())).send();
    }

    @Override
    public TransactionReceipt cancelBooking(String roomId, String timeSlot, String userId) throws Exception{
        if(contract == null){
            throw new ContractNotInitialized();
        }
        return contract.cancelBooking(StringUtils.stringToBytes32(roomId), new BigInteger(timeSlot), Hash.sha3(userId.getBytes())).send();
    }

    @Override
    public List<Integer> getBookings(String roomId) throws Exception {
        Object[] bookings = contract.getBookings(StringUtils.stringToBytes32(roomId)).send().toArray();
        return Arrays.stream(bookings)
                .map(o -> ((BigInteger)o).intValue())
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> getUserBookings(String roomId, String userId) throws Exception {
        Object[] bookings = contract.getUserBookings(StringUtils.stringToBytes32(roomId), Hash.sha3(userId.getBytes())).send().toArray();
        return Arrays.stream(bookings)
                .map(o -> ((BigInteger)o).intValue())
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getAllRooms() throws Exception {
        Object[] rooms = contract.getAllRooms().send().toArray();
        return Arrays.stream(rooms)
                .map(o -> StringUtils.hexStringToAscii(Hex.toHexString((byte[])o)))
                .collect(Collectors.toList());
    }
}
