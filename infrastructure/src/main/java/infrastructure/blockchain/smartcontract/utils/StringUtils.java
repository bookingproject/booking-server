package infrastructure.blockchain.smartcontract.utils;

public class StringUtils {

    public static byte[] stringToBytes32(String s){
        byte[] byteValue = s.getBytes();
        byte[] byteValueLen32 = new byte[32];
        System.arraycopy(byteValue, 0, byteValueLen32, 0, byteValue.length);
        return byteValueLen32;
    }

    public static String hexStringToAscii(String hexString){
        String result = "";
        for (int i = 0; i < hexString.length(); i+=2) {
            String b = hexString.substring(i, i+2);
            if(!b.equals("00"))
                result += ((char)Integer.parseInt(b, 16));
        }
        return result;
    }
}
