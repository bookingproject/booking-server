package infrastructure.rest.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.validation.FieldError;

import java.util.List;

@Data
public class JsonError {
    String message;
    List<JsonFieldError> fieldErrorList;

    public JsonError(String message) {
        this.message = message;
    }

    public JsonError(String message, List<JsonFieldError> fieldErrorList) {
        this.message = message;
        this.fieldErrorList = fieldErrorList;
    }
}
