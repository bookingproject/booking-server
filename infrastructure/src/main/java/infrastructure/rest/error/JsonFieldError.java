package infrastructure.rest.error;

import lombok.Data;
import org.springframework.validation.FieldError;

import java.util.List;

@Data
public class JsonFieldError {
    String defaultMessage;
    String field;

    public JsonFieldError(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }

    public JsonFieldError(String defaultMessage, String field) {
        this.defaultMessage = defaultMessage;
        this.field = field;
    }
}
