package infrastructure.rest.error;

import domain.booking.error.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ErrorControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorControllerAdvice.class);

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public JsonError validationException(MethodArgumentNotValidException e) {
        LOGGER.warn("validationException : {}", e.getMessage());

        List<JsonFieldError> collect = e.getBindingResult().getFieldErrors()
                .stream()
                .map(field -> new JsonFieldError(field.getDefaultMessage(), field.getField()))
                .collect(Collectors.toList());

        return new JsonError("Validation error", collect);
    }


    @ExceptionHandler({BookingAlreadyExists.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    public JsonError bookingAlreadyExists(BookingAlreadyExists e) {
        LOGGER.warn("bookingAlreadyExists  : {}", e.getMessage());
        return new JsonError(e.getMessage());
    }

    @ExceptionHandler({BookingNotFound.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public JsonError bookingNotFound(BookingNotFound e) {
        LOGGER.warn("BookingNotFound : {}", e.getMessage());
        return new JsonError(e.getMessage());
    }

    @ExceptionHandler({UnauthorizedOperation.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public JsonError unauthorizedOperation(UnauthorizedOperation e) {
        LOGGER.warn("unauthorizedOperation : {}", e.getMessage());
        return new JsonError(e.getMessage());
    }

    @ExceptionHandler({BookingWrongOwner.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public JsonError bookingWrongOwner(BookingWrongOwner e) {
        LOGGER.warn("BookingWrongOwner : {}", e.getMessage());
        return new JsonError(e.getMessage());
    }

    @ExceptionHandler({TransactionError.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public JsonError transactionError(TransactionError e) {
        LOGGER.warn("TransactionError : {}", e.getMessage());
        return new JsonError(e.getMessage());
    }
}
