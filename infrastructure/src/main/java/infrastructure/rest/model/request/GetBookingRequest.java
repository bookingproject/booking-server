package infrastructure.rest.model.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class GetBookingRequest {

    @NotNull
    @Size(min = 3, max = 3)
    private String roomId;

    @Email
    private String userEmail;
}


