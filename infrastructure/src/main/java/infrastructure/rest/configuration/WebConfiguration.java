package infrastructure.rest.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@Data
public class WebConfiguration implements WebMvcConfigurer {

    @Value("${INFURA_API_KEY}")
    public String infuraApiKey;

    @Value("${WALLET_PRIVATE_KEY}")
    public String walletPrivateKey;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }


}
