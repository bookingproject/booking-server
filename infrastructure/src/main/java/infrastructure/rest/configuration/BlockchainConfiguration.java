package infrastructure.rest.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

@Configuration
public class BlockchainConfiguration {

    private final WebConfiguration webConfiguration;

    public BlockchainConfiguration(WebConfiguration webConfiguration) {
        this.webConfiguration = webConfiguration;
    }

    @Bean
    public Web3j web3j(){
        return Web3j.build(new HttpService("https://rinkeby.infura.io/v3/" + webConfiguration.getInfuraApiKey()));
    }

    @Bean
    public Credentials credentials(){
        return Credentials.create(webConfiguration.getWalletPrivateKey());
    }
}
