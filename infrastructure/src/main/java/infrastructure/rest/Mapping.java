package infrastructure.rest;

public abstract class Mapping {

    public static final String HEALTH = "/health";

    public static class Booking {
        public static final String ROOT_BOOKING = "/";
        public static final String ADD_BOOKING = "/add";
        public static final String CANCEL_BOOKING = "/cancel";
        public static final String GET_ROOMS = "/getRooms";
        public static final String GET_BOOKINGS = "/getBookings";
    }
}
