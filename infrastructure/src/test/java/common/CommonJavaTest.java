package common;

import infrastructure.blockchain.smartcontract.utils.StringUtils;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Test;
import org.web3j.abi.datatypes.generated.Bytes32;
import org.web3j.crypto.Hash;

public class CommonJavaTest {

    @Test
    public void testBytes32(){
        String id = "C01";
        byte[] byteValue = id.getBytes();
        byte[] byteValueLen32 = new byte[32];
        System.arraycopy(byteValue, 0, byteValueLen32, 0, byteValue.length);
        System.out.println("byteId : " + byteValueLen32.length);
        System.out.println("byteId : " + Hex.toHexString(byteValueLen32));
        Bytes32 bytes32 = new Bytes32(byteValueLen32);

    }

    @Test
    public void testBytes32ToString(){
        String id = "C01";
        byte[] byteValue = id.getBytes();
        byte[] byteValueLen32 = new byte[32];
        System.arraycopy(byteValue, 0, byteValueLen32, 0, byteValue.length);
        System.out.println("byteId : " + byteValueLen32.length);
        System.out.println("byteId : " + Hex.toHexString(byteValueLen32));
        Bytes32 bytes32 = new Bytes32(byteValueLen32);
        String s = Hex.toHexString(bytes32.getValue());
        System.out.println("bytes32 : " + StringUtils.hexStringToAscii(s));
    }
}
