package usecase;

import domain.booking.blockchain.ContractService;
import domain.booking.error.*;
import domain.booking.command.AddBookingCommand;
import domain.booking.command.CancelBookingCommand;
import domain.booking.command.GetBookingCommand;
import org.slf4j.LoggerFactory;

import org.slf4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.web3j.protocol.core.methods.response.TransactionReceipt;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookingService {

    public final static String TRANSACTION_SUCCESS = "0x1";
    public final static String TRANSACTION_FAILURE = "0x0";

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingService.class);

    private final ContractService contractService;

    public BookingService(ContractService contractService) {
        this.contractService = contractService;
    }

    public String addBooking(AddBookingCommand command) {
        LOGGER.info("Entering bookRoom with args : {}", command);
        if(isAlreadyBooked(command.getRoomId(), command.getTimeSlot())){
            throw new BookingAlreadyExists("This room is already booked at this timeslot");
        }
        try {
            TransactionReceipt transactionReceipt = contractService.addBooking(command.getRoomId(), command.getTimeSlot(), command.getUserEmail());
            if(transactionReceipt.getStatus().equals(TRANSACTION_SUCCESS)){
                return "Your booking request has been approved";
            }
        } catch(Exception e){
            LOGGER.error("addBooking exception : {}", e.getMessage());
            throw new TransactionError("addBooking : " + e.getMessage());
        }
        throw new UnauthorizedOperation("addBooking not authorized");
    }

    public String cancelBooking(CancelBookingCommand command) {
        LOGGER.info("Entering cancelBooking with args : {}", command);

        if(!isAlreadyBooked(command.getRoomId(), command.getTimeSlot())){
            throw new BookingNotFound("This room is not booked at this timeslot");
        }
        if(!isBookingOwner(command.getRoomId(), command.getTimeSlot(), command.getUserEmail())){
            throw new BookingWrongOwner(command.getUserEmail() + " is not the owner of this booking");
        }

        try {
            TransactionReceipt transactionReceipt = contractService.cancelBooking(command.getRoomId(), command.getTimeSlot(), command.getUserEmail());
            if(transactionReceipt.getStatus().equals(TRANSACTION_SUCCESS)){
                return "Your cancel booking request has been approved";
            }
        } catch(Exception e){
            LOGGER.error("cancelBooking exception : {}", e.getMessage());
            throw new TransactionError("cancelBooking : " + e.getMessage());
        }
        throw new UnauthorizedOperation("cancelBooking not authorized");
    }

    public List<Integer> getBookings(GetBookingCommand command) {
        LOGGER.info("Entering getBookings with args : {}", command);
        List<Integer> bookings = new ArrayList<>();
        try {
            if(command.getUserEmail() != null && !command.getUserEmail().isEmpty()){
                bookings = contractService.getUserBookings(command.getRoomId(), command.getUserEmail());
            }else{
                bookings = contractService.getBookings(command.getRoomId());
            }
            LOGGER.info("getBookings : {}", bookings.size());
        } catch(Exception e){
            LOGGER.error("getBookings exception : {}", e.getMessage());
            throw new TransactionError("getBookings : " + e.getMessage());
        }
        return bookings;
    }

    public List<String> getRooms() {
        LOGGER.info("Entering getRooms with args");
        List<String> rooms = new ArrayList<>();
        try {
            rooms = contractService.getAllRooms();
            LOGGER.info("getRooms : {}", rooms.size());
        } catch(Exception e){
            LOGGER.error("getRooms exception : {}", e.getMessage());
            throw new TransactionError("getRooms : " + e.getMessage());
        }
        return rooms;
    }

    private boolean isAlreadyBooked(String roomId, String timeSlot){
        try {
            List<Integer> bookings = contractService.getBookings(roomId);
            return (bookings.contains(Integer.parseInt(timeSlot)));
        } catch(Exception e){
            LOGGER.error("isAlreadyBooked exception : {}", e.getMessage());
        }
        return false;
    }

    private boolean isBookingOwner(String roomId, String timeSlot, String userEmail){
        try {
            List<Integer> bookings = contractService.getUserBookings(roomId, userEmail);
            return (bookings.contains(Integer.parseInt(timeSlot)));
        } catch(Exception e){
            LOGGER.error("isBookingOwner exception : {}", e.getMessage());
        }
        return false;
    }
}
