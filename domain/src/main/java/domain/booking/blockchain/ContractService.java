package domain.booking.blockchain;

import org.web3j.protocol.core.methods.response.TransactionReceipt;

import java.util.List;

public interface ContractService {

    TransactionReceipt addBooking(String roomId, String timeSlot, String userId)throws Exception;
    TransactionReceipt cancelBooking(String roomId, String timeSlot, String userId)throws Exception;
    List<Integer> getBookings(String roomId)throws Exception;
    List<Integer> getUserBookings(String roomId, String userId)throws Exception;
    List<String> getAllRooms()throws Exception;
}
