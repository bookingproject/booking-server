package domain.booking.blockchain;

import java.util.concurrent.Future;

public interface FutureCallback {

    void isDone(Future<?> future);

}
