package domain.booking.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class AddBookingCommand {

    private String userEmail;
    private String roomId;
    private String timeSlot;
}
