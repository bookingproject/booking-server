package domain.booking.error;

public class BookingNotFound extends RuntimeException{

    public BookingNotFound(String message){
        super(message);
    }
}
